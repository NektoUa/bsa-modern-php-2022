<?php

declare(strict_types=1);

namespace App\Task1;

class Track
{
    public $cars = [];

    public function __construct(float $lapLength, int $lapsNumber)
    {
        $this->lapLength = $lapLength;
        $this->lapsNumber = $lapsNumber;
        //@todo
    }

    public function getLapLength(): float
    {
        return $this->lapLength;
    }

    public function getLapsNumber(): int
    {
        return $this->lapsNumber;
    }

    public function add(Car $car): void
    {
        $this->cars[] = $car;
    }

    public function all(): array
    {
        return $this->cars;
    }

    public function run(): Car
    {
        $time = '';
        $winner = '';
        $track = ceil($this->getLapLength() * $this->getLapsNumber());
        foreach ($this->cars as $car) {
            $distance = ($car->getFuelTankVolume() / $car->getFuelConsumption()) * 100000;
            $pitStop = ($track / $distance) * $car->getPitStopTime();
            $carTime = ($track / $car->getSpeed()) + $pitStop;
            if (!$time || $time > $carTime) {
                $time = $carTime;
                $winner = $car;
            }
        }

        return $winner;
    }
}
