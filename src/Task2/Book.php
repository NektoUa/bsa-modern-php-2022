<?php

declare(strict_types=1);

namespace App\Task2;

class Book
{
    public function __construct($title, $price, $pagesNumber)
    {
        $this->title = $title;
        $this->price = $price;
        $this->pagesNumber = $pagesNumber;

        if ($price <= 0 || gettype($price) != "integer") {
            throw new \Exception("Where is a price?");
        }
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getPagesNumber(): int
    {
        return $this->pagesNumber;
    }
}
